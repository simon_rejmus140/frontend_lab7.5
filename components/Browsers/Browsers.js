import React from 'react';
import './Browsers.scss';

const Browsers = ({ input }) => {
  const browsers = [];

  for (const element in input) {
    const { name, url, description } = input[element];

    browsers.push(
      <div key={name} class="brick">
        <h3>{name}</h3>
        <img src={url} ale={name} />
        <p>{description}</p>
      </div>
    );
  }

  return (
    <React.Fragment>
      <h1>Popular web browsers</h1>
      {browsers}
    </React.Fragment>
  );
};

export default Browsers;
