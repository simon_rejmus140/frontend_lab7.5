import React from 'react';
import Header from '../components/Header/Header';
import Browsers from '../components/Browsers/Browsers';
import Footer from '../components/Footer/Footer';
import './index.scss';
import '../components/Browsers/Browsers.scss';

const browsersData = {
  'Mozilla Firefox': {
    name: 'Mozilla Firefox',
    url: 'https://upload.wikimedia.org/wikipedia/commons/thumb/7/76/Mozilla_Firefox_logo_2013.svg/1200px-Mozilla_Firefox_logo_2013.svg.png',
    description:
      'Mozilla Firefox is an open-source web browser developed by Mozilla. Firefox has been the second most popular web browser since January, 2018.',
  },
  'Google Chrome': {
    name: 'Google Chrome',
    url: 'https://upload.wikimedia.org/wikipedia/commons/e/e1/Google_Chrome_icon_%28February_2022%29.svg',
    description:
      "Google Chrome is a web browser developed by Google, released in 2008. Chrome is the wordl's most popular browser today!",
  },
  'Microsoft Edge': {
    name: 'Microsoft Edge',
    url: 'https://upload.wikimedia.org/wikipedia/commons/thumb/9/98/Microsoft_Edge_logo_%282019%29.svg/2048px-Microsoft_Edge_logo_%282019%29.svg.png',
    description:
      'Microsoft Edge is a web browser developed by Microsoft, released in 2015. Microsoft Edge replaced Internet Explorer.',
  },
};

export default function App() {
  return (
    <React.Fragment>
      <Header />
      <Browsers input={browsersData} />
      <Footer />
    </React.Fragment>
  );
}
